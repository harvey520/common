//
//  MD5.swift
//  MyBase
//
//  Created by Harvey on 2023/12/2.
//

import CryptoKit

public extension String {
    
    var md5: String {
        data?.md5 ?? ""
    }
    
    /// 大写
    var upper: String {
        uppercased()
    }
    
    /// 小写
    var lower: String {
        lowercased()
    }
}

public extension Data {
    
    var md5: String {
        let hash = Insecure.MD5.hash(data: self)
        return hash.map { String(format: "%02.2hhx", $0) }.joined()
    }
}
