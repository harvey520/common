//
//  Broadcast.swift
//  MyBase
//
//  Created by yaozuopan on 2024/2/4.
//

import Foundation
import RxSwift

/// 广播
public struct Broadcast { }

/// Represents an object that is both an observable sequence as well as an observer.
public extension Broadcast {
    
    /// Each notification is broadcasted to all subscribed observers.
    static let examplePublish = PublishSubject<Void>()
    /// Each notification is broadcasted to all subscribed and future observers, subject to buffer trimming policies.
    static let exampleReplay = ReplaySubject<Int>.create(bufferSize: 1)
    /// Observers can subscribe to the subject to receive the last (or initial) value and all subsequent notifications.
    static let exampleBehavior = BehaviorSubject<Int>(value: 0)
}

public extension ObservableType {
    
    /// Subscribes an element handler to an observable sequence.
    ///
    /// - Parameters:
    ///   - bag: Adds self to bag
    ///   - onNext: Action to invoke for each element in the observable sequence.
    func listen(_ bag: DisposeBag, onNext: @escaping (Element) -> Void) {
        subscribe(onNext: onNext).disposed(by: bag)
    }
    
    func listen(onNext: @escaping (Element) -> Void) -> Disposable {
        return subscribe(onNext: onNext)
    }
    
    /// Wraps the source sequence in order to run its observer callbacks on main scheduler.
    func observeOnMain() -> Observable<Element> {
        return observe(on: MainScheduler.instance)
    }
}

public extension ObserverType where Element == () {
    
    func onNext() {
        onNext(())
    }
}
