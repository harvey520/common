//
//  Dictionary.swift
//  MyBase
//
//  Created by Harvey on 2021/5/18.
//

import Foundation

// MARK: - Extension Dictionary
public extension Dictionary {
    
    static func + (base: Self, additional: Self) -> Self {
        return Dictionary(
            base.map({ ($0.0, $0.1) }) +
            additional.map({ ($0.0, $0.1) })) { _, last in last }
    }
}

public extension Dictionary.Keys {
    var items: [Element] { map { $0 } }
}

public extension Dictionary.Values {
    var items: [Element] { map { $0 } }
}
