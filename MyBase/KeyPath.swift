//
//  KeyPath.swift
//  MyBase
//
//  Created by yaozuopan on 2023/11/28.
//

import Foundation

public struct KeyPathAccess<Root, Value> {
    public let id: KeyPath<Root, Value>
    public let value: Value
    
    public init(_ id: KeyPath<Root, Value>, value: Value) {
        self.value = value
        self.id = id
    }
    
    public init(_ id: KeyPath<Root, Value>, equal: Value) {
        self.value = equal
        self.id = id
    }
}

public struct KeyPathWritable<Root, Value> {
    public let id: WritableKeyPath<Root, Value>
    public let value: Value
    
    public init(_ id: WritableKeyPath<Root, Value>, value: Value) {
        self.value = value
        self.id = id
    }
    
    public func write(to root: inout Root) {
        root[keyPath: id] = value
    }
    
    public func read(from root: Root) -> Value {
        return root[keyPath: id]
    }
}
