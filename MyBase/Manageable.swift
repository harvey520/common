//
//  ManageableFolder.swift
//  MyBase
//
//  Created by Harvey on 2024/3/2.
//

import Foundation

private let storager = FileManager.default

/// 目录管理协议
public protocol Manageable {
    /// 目录
    var folder: String { get }
    /// 后缀名
    var suffix: String { get }
}

public extension Manageable {
    
    private func filePath(_ name: String) -> String {
        folder + "/\(name).\(suffix)"
    }
    
    /// 如果不存在则创建目录
    func createFolder() {
        FilePath(folder, isDirectory: true).create()
    }
    
    /// 删除指定文件
    ///
    /// - Parameter fileName: 文件名(不包括后缀)
    func delete(_ fileName: String) {
        do {
            try storager.removeItem(atPath: filePath(fileName))
        } catch {
            Logger.error(error)
        }
    }
    
    /// 移除文件夹
    ///
    /// - Parameters:
    ///   - filePath: 完整源路径
    ///   - fileName: 文件名(不包括后缀)，移动到当前目录，以 fileName 保存
    func move(from filePath: String, to fileName: String) {
        do {
            try storager.moveItem(atPath: filePath, toPath: self.filePath(fileName))
        } catch {
            Logger.error(error)
        }
    }
    
    /// 删除所有文件
    func deleteAll() {
        do {
            try storager.removeItem(atPath: folder)
        } catch {
            Logger.error(error)
        }
    }
    
    /// 检测指定文件是否存在
    ///
    /// - Parameter fileName: 文件名(不包括后缀)
    func exist(of fileName: String) -> Bool {
        return storager.fileExists(atPath: filePath(fileName))
    }
}
