//
//  URLString.swift
//  MyBase
//
//  Created by Harvey on 2024/1/7.
//

import Foundation

/// URL or String
public protocol URLString {
    var value: URL? { get }
}

extension String: URLString {
    public var value: URL? { URL(string: self) }
}

extension URL: URLString {
    public var value: URL? { self }
}

public extension URL {
    
    var queryItems: [String: String] {
        guard let component = URLComponents(url: self, resolvingAgainstBaseURL: false) else { return [:] }
        guard isNotEmpty(component.queryItems) else { return [:] }
        let keyValues = component
            .queryItems!
            .map { ($0.name, $0.value ?? "") }
        return Dictionary(uniqueKeysWithValues: keyValues)
    }
}
