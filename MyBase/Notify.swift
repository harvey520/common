//
//  Notify.swift
//  
//
//  Created by Harvey on 2020/1/5.
//  Copyright © 2020 姚作潘/Harvey. All rights reserved.
//

import Foundation

fileprivate let center = NotificationCenter.default

fileprivate class WeaklyObject: NSObject {
    weak var owner: NSObjectProtocol?
    var delegateObservers: [NSObjectProtocol]
    init(_ owner: NSObjectProtocol, delegateObservers: [NSObjectProtocol] = []) {
        self.owner = owner
        self.delegateObservers = delegateObservers
    }
}

extension Notify {
    
    fileprivate struct Keeper {
        static var observers: [WeaklyObject] = []
        static func remove(_ observer: NSObjectProtocol) {
            observers
                .filter { $0.owner === observer || isNil($0.owner) }
                .forEach {
                    $0.delegateObservers.forEach { center.removeObserver($0) }
                }
            observers.removeAll { isNil($0.owner) || $0.owner === observer }
        }
        
        static func append(_ delegateObserver: NSObjectProtocol, owner: NSObjectProtocol) {
            guard let weakly = observers.filter({ $0.owner === owner }).first else {
                observers.append(WeaklyObject(owner, delegateObservers: [delegateObserver]))
                return
            }
            weakly.delegateObservers.append(delegateObserver)
        }
    }
    
    public struct Name {
        fileprivate let rawValue: String
        public init(_ rawValue: String) {
            self.rawValue = rawValue
        }
    }
}

public class Notify {
    
    public init() {}
    
    private(set) var name: Notification.Name!
    @discardableResult
    public func name(_ value: Notification.Name) -> Self {
        name = value
        return self
    }
    
    @discardableResult
    public func name(_ value: Notify.Name) -> Self {
        name = Notification.Name(rawValue: value.rawValue)
        return self
    }
    
    private(set) var observer: NSObjectProtocol!
    @discardableResult
    public func observer(_ value: NSObjectProtocol) -> Self {
        observer = value
        return self
    }
    
    private(set) var queue: OperationQueue = .main
    @discardableResult
    public func queue(_ value: OperationQueue) -> Self {
        queue = value
        return self
    }
    
    private(set) var object: Any?
    @discardableResult
    public func object(_ value: Any) -> Self {
        object = value
        return self
    }
    
    public func subscribe(_ value: @escaping (Notification) -> Void) {
        if isNil(observer) || isNil(name) {
            fatalError("observer or name must be not nil")
        }
        
        let delegateObserver = center
            .addObserver(forName: name,
                         object: object,
                         queue: queue,
                         using: value)
        Keeper.append(delegateObserver, owner: observer)
    }
    
    public static func remove(_ observer: NSObjectProtocol) {
        center.removeObserver(observer)
        Keeper.remove(observer)
    }
}

public extension Notify {
    
    static func post(_ name: Notification.Name,
                     object: Any? = nil,
                     userInfo: [AnyHashable : Any] = [:]) {
        center.post(name: name, object: object, userInfo: userInfo)
    }
    
    static func post(_ name: Notify.Name,
                     object: Any? = nil,
                     userInfo: [AnyHashable : Any] = [:]) {
        center.post(name: Notification.Name(rawValue: name.rawValue),
                    object: object, userInfo: userInfo)
    }
}
