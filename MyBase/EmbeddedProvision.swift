//
//  EmbeddedProvision.swift
//  MyBase
//
//  Created by Harvey on 2023/12/23.
//

import Foundation

public struct EmbeddedProvision: Codable {
    /// mobile provision Name
    public private(set) var name: String
    public private(set) var version: Int
    /// mobile provision UUID
    public private(set) var uuid: String
    public private(set) var timeToLive: Int
    /// 团队ID
    public private(set) var teamID: [String]
    /// 团队名称
    public private(set) var teamName: String
    /// 应用ID 名称
    public private(set) var appIDName: String
    /// 支持平台
    public private(set) var platform: [String]
    /// 是否由 Xcode 管理
    public private(set) var isXcodeManaged: Bool
    /// 创建时间
    public private(set) var creationDate: String
    /// 应用 ID 前辍
    public private(set) var appIDPrefix: [String]
    /// 过期时间
    public private(set) var expirationDate: String
    public private(set) var entitlements: Entitlements
    /// 注册设备列表
    public private(set) var provisionedDevices: [String]?

    enum CodingKeys: String, CodingKey {
        case name = "Name"
        case uuid = "UUID"
        case version = "Version"
        case platform = "Platform"
        case teamName = "TeamName"
        case appIDName = "AppIDName"
        case teamID = "TeamIdentifier"
        case timeToLive = "TimeToLive"
        case entitlements = "Entitlements"
        case creationDate = "CreationDate"
        case isXcodeManaged = "IsXcodeManaged"
        case expirationDate = "ExpirationDate"
        case provisionedDevices = "ProvisionedDevices"
        case appIDPrefix = "ApplicationIdentifierPrefix"
    }
    
    static let embeddedProvision: EmbeddedProvision? = { () -> EmbeddedProvision? in
        guard let path = App.Bundle.embedded else {
            Logger.verbose("embedded.mobileprovision NOT FOUND")
            return nil
        }
        
        let embeddedPlistPath = App.Path.temporary + "/embedded.plist"
        if embeddedPlistPath.filePath().isExists == false {
            guard let data = try? Data(contentsOf: path.fileURL!) else { return nil }
            guard let string = String(data: data, encoding: .ascii) else { return nil }
            guard let string = string.components(separatedBy: "</plist>").first else { return nil }
            guard var string = string.components(separatedBy: "<?xml").last else { return nil }
            
            string = "<?xml" + string + "</plist>"
            try? string.data?.write(to: embeddedPlistPath.fileURL!)
        }
        
        guard let embedded = try? NSDictionary(contentsOf: embeddedPlistPath.fileURL!, error: ()) else { return nil }
        guard var object = embedded as? [String: Any] else { return nil }
        
        object.removeValue(forKey: "DER-Encoded-Profile")
        object.removeValue(forKey: "DeveloperCertificates")
        
        if let ereationDate = object["CreationDate"] {
            object["CreationDate"] = "\(ereationDate)"
        }
        
        if let expirationDate = object["ExpirationDate"] {
            object["ExpirationDate"] = "\(expirationDate)"
        }
        
        guard JSONSerialization.isValidJSONObject(object) else { return nil }
        
        do {
            let json = try JSONSerialization.data(withJSONObject: object)
            let embeddedProvision = try JSONDecoder().decode(EmbeddedProvision.self, from: json)
            return embeddedProvision
        } catch {
            Logger.error(error)
        }
        return nil
    }()
}

public struct Entitlements: Codable {

    /// App ID
    public private(set) var appID: String
    /// 团队ID
    public private(set) var teamID: String
    /// 发布应用永远为 false
    public private(set) var getTaskAllow: Bool
    public private(set) var appGroups: [String]?
    /// App 发布类型
    public private(set) var apsEnvironment: String?
    public private(set) var networkingWiFiInfo: Bool?
    public private(set) var keychainAccessGroups: [String]
    
    enum CodingKeys: String, CodingKey {
        case getTaskAllow = "get-task-allow"
        case appID = "application-identifier"
        case apsEnvironment = "aps-environment"
        case keychainAccessGroups = "keychain-access-groups"
        case appGroups = "com.apple.security.application-groups"
        case teamID = "com.apple.developer.team-identifier"
        case networkingWiFiInfo = "com.apple.developer.networking.wifi-info"
    }
}
