//
//  String.swift
//  MyBase
//
//  Created by Harvey on 2021/5/18.
//

import UIKit

// MARK: - Extension String
public extension String {
    
    /// 替换
    /// - Parameter replaceItems: [oldValue: newValue]
    /// - Returns: String
    func replace(_ replaceItems: [String: String]) -> String {
        var pointee = self
        replaceItems.forEach { (oldValue, newValue) in
            pointee = pointee.replacingOccurrences(of: oldValue, with: newValue)
        }
        return pointee
    }
    
    /// 替换
    func replace(_ old: String, _ new: String) -> String {
        return replacingOccurrences(of: old, with: new)
    }
    
    /// 生成随机字符串
    ///
    /// - Parameters:
    ///   - count: 生成字符串长度
    ///   - isLetter: false=大小写字母和数字组成，true=大小写字母组成，默认为false
    /// - Returns: String
    static func random(_ count: Int, _ isLetter: Bool = false) -> String {
        
        // swiftlint:disable:next identifier_name
        var ch: [CChar] = Array(repeating: 0, count: count)
        for index in 0..<count {
            
            var num = isLetter ? arc4random_uniform(58)+65:arc4random_uniform(75)+48
            if num > 57 && num < 65 && isLetter == false {
                num = num % 57 + 48
            } else if num > 90 && num < 97 { num = num % 90 + 65 }
            
            ch[index] = CChar(num)
        }
        
        return "\(String(cString: ch).prefix(count))"
    }
    
    /// start 表示开始下标， length 截取长度
    subscript(start: Int, length: Int) -> String {
        let start = index(startIndex, offsetBy: start)
        let end = index(start, offsetBy: length)
        return "\(self[start..<end])"
    }
    
    /// 元组第一个元素为开始位置; 第二个元素，1表示正坐标，其他表示负坐标
    subscript(factor: (Int, Int)) -> String {
        if factor.1 == 1 {
            let start = index(startIndex, offsetBy: factor.0)
            return "\(self[start..<endIndex])"
        }
        
        let end = index(startIndex, offsetBy: factor.0)
        return "\(self[startIndex..<end])"
    }
    
    /// 获取 NSRange
    /// - Parameters:
    ///   - start: 开始下标, 默认 0,可选
    ///   - end:  结束下标, 默认 nil(到字符串最后),可选
    /// - Returns: NSRange
    func range(_ start: Int = 0, _ end: Int? = nil) -> NSRange {
        var endIndex: Int = count - 1
        if let end = end {
            endIndex = end
        }
        return NSRange(location: start, length: endIndex - start + 1)
    }

    /// 转为 File URL
    var fileURL: URL? { URL(fileURLWithPath: self) }
    
    func filePath(_ isDirectory: Bool = false) -> FilePath {
        return FilePath(self, isDirectory: isDirectory)
    }

    /// 转为 URL
    var url: URL? { URL(string: self) }
    
    /// 通过图片名称实例 UIImage
    var image: UIImage? { UIImage(named: self) }
    
    /// to Data with UTF-8 encoding
    var data: Data? { data(using: .utf8) }
}

public extension String {
    
    /// 本地时间时间转换成时间戳
    func timeIntervalSince1970(_ format: Date.Format = .middle) -> TimeInterval {
        Date.formatter.dateFormat = format.rawValue
        return Date.formatter.date(from: self)?.timeIntervalSince1970 ?? 0.0
    }
    
    /// 本地时间时间转换成 Date
    func date(_ format: Date.Format = .middle) -> Date? {
        Date.formatter.dateFormat = format.rawValue
        return Date.formatter.date(from: self)
    }
}

extension String {
    
    private static let hexMatch: [String: Int] = [
        "0": 0,
        "1": 1,
        "2": 2,
        "3": 3,
        "4": 4,
        "5": 5,
        "6": 6,
        "7": 7,
        "8": 8,
        "9": 9,
        "A": 10,
        "B": 11,
        "C": 12,
        "D": 13,
        "E": 14,
        "F": 15
    ]
    /// 将十六进制字符串转换成十进制数值
    public var hexToDecimal: Int {
        var decimal = 0
        guard var cchars = cString(using: .utf8) else { return decimal }
        
        cchars.removeLast()
        cchars.reverse()
        
        // 删除`#`
        if cchars.last == 35 {
            cchars.removeLast()
        }
        
        for (index, char) in cchars.enumerated() {
            let key = String(format: "%c", char).uppercased()
            let value = String.hexMatch[key] ?? 0

            if index == 0 {
                decimal = value
                continue
            }
            
            let powValue = pow(16.0, Double(index)).int * value
            decimal += powValue
        }
        return decimal
    }
}

public extension String {
    /// URL 转义
    func escaped(_ characterSet: CharacterSet = .urlQueryAllowed) -> Self? {
        return addingPercentEncoding(withAllowedCharacters: characterSet)
    }
}

public extension Substring {
    
    var string: String {
       String(self)
    }
}

public extension String {
    
    static func uuid(_ suffix: String = "") -> String {
        UUID().uuidString.replace("-", "").appending(suffix)
    }
}

public extension String {
    
    /// 从字符串中提取数字
    ///
    /// # Example
    /// ``` swift
    /// let string = "ab.cde188.9fg7h8f85f55fd55d5.03"
    /// let numbers = string.extractNumbers()
    /// print(numbers) // ["188.9", "7", "8", "85", "55", "55", "5.03"]
    /// ```
    func extractNumbers() -> [String] {
        let pattern = #"\d+\.?\d{0,}"#
        guard let regular = try? NSRegularExpression(pattern: pattern) else { return [] }
        let matches = regular.matches(in: self, range: NSRange(location: 0, length: count))
        return matches.compactMap { String(self[Range($0.range, in: self)!]) }
    }
    
    /// Returns a new string made by removing from both ends of the String characters contained in a given character set.
    ///
    /// - Parameter characterSet: default is whitespacesAndNewlines
    /// - Returns: new string
    func trimming(in characterSet: CharacterSet = .whitespacesAndNewlines) -> Self {
        return trimmingCharacters(in: characterSet)
    }
}
