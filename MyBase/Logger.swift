//
//  Logger.swift
//
//
//  Created by Harvey on 2021/4/27.
//

import Foundation

/// 日记管理
class Logger {
    
    static var isDebug: Bool = false
    static var saveHandler: ((_ value: String, _ type: Logger.`Type`) -> Void)?
    static var allowTypes: [Logger.`Type`] = Logger.aboveNetwork
    
    /// 当为 true 时，不打印文件名、方法名、行号
    static var isConciseLog: Bool = false
    
    fileprivate static let shared = Logger()
    private init() { }
    
    fileprivate var dateString: String { Date().format(.full) }
}

extension Logger {
    enum `Type`: Int {
        case verbose = 300
        case debug = 400
        case info = 800
        case warning = 900
        case socket = 950
        case bluetooth = 955
        case network = 1000
        case error = 1400
        
        var name: String {
            switch self {
            case .verbose: return "Verbose"
            case .debug: return "Debug"
            case .info: return "Info"
            case .warning: return "Warning"
            case .socket: return "Socket"
            case .bluetooth: return "Bluetooth"
            case .network: return "Network"
            case .error: return "Error"
            }
        }
    }
    
    /// 打印所有日记
    static let all: [Logger.`Type`] = [.verbose, .debug, .info, .warning, .socket, .bluetooth, .network, .error]
    /// 打印级别比 **verbose** 高的日记
    static let aboveVerbose: [Logger.`Type`] = [.debug, .info, .warning, .socket, .bluetooth, .network, .error]
    /// 打印级别比 **debug** 高的日记
    static let aboveDebug: [Logger.`Type`] = [.info, .warning, .socket, .bluetooth, .network, .error]
    /// 打印级别比 **info** 高的日记
    static let aboveInfo: [Logger.`Type`] = [.warning, .socket, .bluetooth, .network, .error]
    /// 打印级别比 **warn** 高的日记
    static let aboveWarn: [Logger.`Type`] = [ .socket, .bluetooth, .network, .error]
    /// 打印级别比 **network** 高的日记
    static let aboveNetwork: [Logger.`Type`] = [.error]

}

extension Logger {
    
    static func verbose( _ logs: Any..., file: String = #file, method: String = #function, line: Int = #line) {
        log(logs.map { "\($0)" }.joined(separator: " "), type: .verbose, file: file, method: method, line: line)
    }
    
    static func debug( _ logs: Any..., file: String = #file, method: String = #function, line: Int = #line) {
        log(logs.map { "\($0)" }.joined(separator: " "), type: .debug, file: file, method: method, line: line)
    }
    
    static func info( _ logs: Any..., file: String = #file, method: String = #function, line: Int = #line) {
        log(logs.map { "\($0)" }.joined(separator: " "), type: .info, file: file, method: method, line: line)
    }
    
    static func warning( _ logs: Any..., file: String = #file, method: String = #function, line: Int = #line) {
        log(logs.map { "\($0)" }.joined(separator: " "), type: .warning, file: file, method: method, line: line)
    }
    
    static func socket( _ logs: Any..., file: String = #file, method: String = #function, line: Int = #line) {
        log(logs.map { "\($0)" }.joined(separator: " "), type: .socket, file: file, method: method, line: line)
    }
    
    static func bluetooth( _ logs: Any..., file: String = #file, method: String = #function, line: Int = #line) {
        log(logs.map { "\($0)" }.joined(separator: " "), type: .bluetooth, file: file, method: method, line: line)
    }
    
    static func network( _ logs: Any..., file: String = #file, method: String = #function, line: Int = #line) {
        log(logs.map { "\($0)" }.joined(separator: " "), type: .network, file: file, method: method, line: line)
    }
    
    static func error( _ logs: Any..., file: String = #file, method: String = #function, line: Int = #line) {
        log(logs.map { "\($0)" }.joined(separator: " "), type: .error, file: file, method: method, line: line)
    }
    
    private static func log(
        _ log: String,
        type: Logger.`Type`,
        file: String ,
        method: String,
        line: Int,
        separator: String = " ",
        terminator: String = "\n") {
            objc_sync_enter(Logger.shared)
            
            let dateString = "\(Logger.shared.dateString) \(file.split(separator: "/").last!) \(method) [Line \(line)] \(type.name):"
            let formatLog = (Logger.isConciseLog ?  Logger.shared.dateString : dateString) + separator + log
            if Logger.isDebug, Logger.allowTypes.contains(type) {
                print(formatLog, terminator: terminator)
            }
            
            Logger.saveHandler?(formatLog, type)
            
            objc_sync_exit(Logger.shared)
        }
}
