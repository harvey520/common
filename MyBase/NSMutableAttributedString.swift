//
//  NSMutableAttributedString.swift
//  MyBase
//
//  Created by Harvey on 2021/5/18.
//

import Foundation

// MARK: - Extension NSMutableAttributedString
extension NSMutableAttributedString {
    
    /// 获取 NSRange
    /// - Parameters:
    ///   - start: Int, 默认 nil,可选
    ///   - end:  Int, 默认 nil,可选
    /// - Returns: NSRange
    public func range(_ start: Int = 0, _ end: Int? = nil) -> NSRange {
        return string.range(start, end)
    }
}

public extension String {
    
    /// 计算字符串显示Size
    @available(*, deprecated, message: "Use size(_:attributes:options:) instead.")
    func box(_ limitWidth: CGFloat = .infinity,
             limitHeight: CGFloat = .infinity,
             attributes: [NSAttributedString.Key: Any] = [:]) -> (CGSize, NSAttributedString)
    {
        let attributedString = NSMutableAttributedString(string: self)
        attributedString.addAttributes(attributes, range: self.range())
        
        let boundingRect =  attributedString.boundingRect(with: CGSize(width: limitWidth, height: limitHeight), options: .usesLineFragmentOrigin, context: nil)
        return (boundingRect.size.ceil, attributedString)
    }

    /// 对子字符串进行
    @available(*, deprecated, message: "Use AttributedString.add(:) instead.")
    func attributed(_ markValue: String, attributes: [NSAttributedString.Key: Any]) -> NSAttributedString {
        
        let attributedString = NSMutableAttributedString(string: self)
        let rangeIndex = range(of: markValue)!
        
        let attributesRange = NSRange(rangeIndex.lowerBound..<rangeIndex.upperBound, in: self)
        attributedString.addAttributes(attributes, range: attributesRange)
        return attributedString
    }
}
