//
//  Scheduler.swift
//  MyBase
//
//  Created by Harvey on 2024/5/2.
//

import RxSwift

public struct Scheduler { }

public extension Scheduler {
    static let main = MainScheduler.instance
    static let global = ConcurrentDispatchQueueScheduler(queue: .global)
}
