//
//  FileWriter.swift
//  MyBase
//
//  Created by yaozuopan on 2024/7/25.
//

import Foundation

private let manager = FileManager.default
private let formatter = DateFormatter()
private let date = Date()

/// 文件写入管理
public protocol FileWriter: AnyObject {
    associatedtype T: FileWriter
    
    static var shared: T { get }
    
    var folder: String { get }
    var suffix: String { get }
    var maxFileCount: Int { get }
    
    var currentPath: String { get }
    var currentName: String { get }
    
    var fileHandle: FileHandle! { get set }
    
    /// ```
    /// init() {
    ///   prepare()
    /// }
    /// ```
    init()
    
    func fileContent(_ name: String) -> [String]
    
    @discardableResult
    func append(_ value: String?) -> Bool
}

extension FileWriter {
    
    public var maxFileCount: Int { 20 }
    public var suffix: String { "log" }
    public var folder: String {
        NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first! + "/Logger/Page"
    }
    
    /// 当前文件路径
    public var currentPath: String {
        folder + "/\(currentName)"
    }
    
    /// 当前文件名
    public var currentName: String {
        formatter.dateFormat = "yyyy-MM-dd-HHmm"
        return formatter.string(from: date) + ".\(suffix)"
    }
    
    /// 检查文件数量
    private func checkFileCount() {
        let files = fetchFiles()
        
        guard files.count > maxFileCount else { return }
        deleteFile(files[0])
    }
    
    /// 获取文件内容
    public func fileContent(_ name: String) -> [String] {
        let path = folder + "/\(name)"
        let filePath = URL(fileURLWithPath: path)
        guard let string = try? String(contentsOf: filePath, encoding: .utf8) else { return [] }
        return string.components(separatedBy: "\n")
    }
    
    /// 写入内容
    @discardableResult
    public func append(_ value: String?) -> Bool {
        guard let data = value?.data else { return false }
        do {
            try fileHandle.seekToEnd()
            try fileHandle.write(contentsOf: data)
        } catch {
            Logger.error(error)
            return false
        }
        return true
    }
}

// MARK: - Fetch
extension FileWriter {
    
    /// 获取所有文件
    private func fetchFiles() -> [String] {
        do {
            return try manager.subpathsOfDirectory(atPath: folder).sorted()
        }
        catch {
            Logger.error(error)
        }
        
        return []
    }
    
    /// 获取所有文件
    public static func fetchFiles() -> [String] {
        shared.fetchFiles()
    }
}

// MARK: - Delete
extension FileWriter {
    
    /// 删除所有文件
    private func deleteAll() {
        do {
            try manager.removeItem(atPath: folder)
            prepare()
        }
        catch {
            Logger.error(error)
        }
    }
    
    /// 删除指定文件
    private func deleteFile(_ name: String) {
        let path = folder + "/\(name)"
        do {
            try manager.removeItem(atPath: path)
            guard path == currentPath else { return }
            prepare()
        }
        catch {
            Logger.error(error)
        }
    }
    
    /// 删除指定文件
    public static  func deleteFile(_ name: String) {
        shared.deleteFile(name)
    }
    
    /// 删除所有文件
    public static func deleteAll() {
        shared.deleteAll()
    }
    
    /// 删除一个最新的文件
    public static func deleteLast() {
        shared.deleteFile(shared.currentName)
    }
}

// MARK: - Read
extension FileWriter {
    
    /// 获取文件内容
    public static func fileContent(_ name: String) -> [String] {
        return shared.fileContent(name)
    }
    
    /// 获取最新文件内容
    public static func lastContent() -> [String] {
        return shared.fileContent(shared.currentName)
    }
}

// MARK: - Write
extension FileWriter {
    /// 写入内容
    public static func write(_ text: String?) {
        guard let text = text else { return }
        shared.append(text)
    }
}

// MARK: - Create
extension FileWriter {
    
    public func prepare() {
        guard createFolder() else { fatalError("创建目录失败") }
        guard createFile() else { fatalError("创建文件失败") }
        
        fileHandle = .init(forWritingAtPath: currentPath)
        checkFileCount()
    }
    
    /// 创建目录
    @discardableResult
    private func createFolder() -> Bool {
        var isDirectory: ObjCBool = true
        guard manager.fileExists(atPath: folder, isDirectory: &isDirectory) == false else { return true }
        
        do {
            try manager.createDirectory(atPath: folder, withIntermediateDirectories: true)
            return true
        } catch {
            Logger.error(error)
            return false
        }
    }
    
    private func createFile() -> Bool {
        guard manager.fileExists(atPath: currentPath) == false else { return true }
        return manager.createFile(atPath: currentPath, contents: nil)
    }
}
