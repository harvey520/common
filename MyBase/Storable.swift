//
//  Storable.swift
//  MyBase
//
//  Created by Harvey on 2024/3/1.
//

import Foundation

private let storager = UserDefaults.standard

/// 持久性保存协议
public protocol Storable: Codable {
    associatedtype T: Storable
    /// 唯一标识(比如用户ID，用户账号、设备ID等)
    var uuid: String { get }
    init(_ uuid: String)
    
    /// 用于缓存所有实例, 当你不清楚所造成的风险时，请忽直接操作此属性
    static var all: [T] { get set }
    
    /// 如果需要数据隔离(每个用户保存的地方不一样)，应该实现该属性
    static var groupName: String { get }
}

public extension Storable {
    
    static var groupName: String { "\(Self.self)" }
    
    /// 获取所有实例
    static func fetchAll() -> [Self] {
        guard let jsonString = storager.string(forKey: groupName) else { return [] }
        return JSON.decode([Self].self, from: jsonString) ?? []
    }
    
    /// 通过 UUID 获取实例
    static func fetch(_ uuid: String) -> Self.T {
        all.filter({ $0.uuid == uuid }).first ?? Self.T.init(uuid)
    }
    
    /// 删除指定实例
    static func delete(_ uuid: String) {
        Self.all.removeAll(where: { $0.uuid == uuid })
        storager.setValue(Self.all.jsonString, forKey: groupName)
    }
    
    /// 删除所有实例
    static func deleteAll() {
        Self.all = []
        storager.removeObject(forKey: groupName)
    }
    
    /// 保存实例
    func save() {
        Self.all.removeAll { $0.uuid == self.uuid }
        Self.all.append(self as! Self.T)
        storager.setValue(Self.all.jsonString, forKey: Self.groupName)
    }
    
    /// 删除实例
    func delete() {
        Self.delete(uuid)
    }
}
