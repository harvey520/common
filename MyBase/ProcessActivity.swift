//
//  ProcessActivity.swift
//  
//
//  Created by Harvey on 2023/12/16.
//

import Foundation

/// 进程任务(必须在主线程运行)
///
/// - Example
/// 
/// ```Swift
/// Async.main.do {
///     ProcessActivity.begin(reason: "测试后台任务运行时间")
///     sleep(10)
///     ProcessActivity.end()
/// }
/// ```
public class ProcessActivity {
    private static let shared = ProcessActivity()
    private init() { }
    
    private let process = ProcessInfo.processInfo
    private var activityTime: TimeInterval = 0
    private var activity: NSObjectProtocol?
    private var reason: String?
    private func begin(options: ProcessInfo.ActivityOptions = [], reason: String) {
        
        end()
        
        self.reason = reason
        activityTime = Date().timeIntervalSince1970
        activity = process.beginActivity(options: options, reason: reason)
        Logger.verbose("\(reason) Begin.")
    }
    
    private func end() {
        guard let activity = activity else { return }
        process.endActivity(activity)
        Logger.verbose("\(reason!) End(\(Date().timeIntervalSince1970 - activityTime)秒).")
    }
    
    public static func begin(options: ProcessInfo.ActivityOptions = [.background], reason: String) {
        shared.begin(options: options, reason: reason)
    }
    
    public static func end() {
        shared.end()
    }
}
