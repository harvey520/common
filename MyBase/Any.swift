//
//  Any.swift
//  MyBase
//
//  Created by Harvey on 2021/6/2.
//

import Foundation

/// 判断可选类型是否为 nil
public func isNil(_ any: Any?) -> Bool {
    switch any {
    case .none: return true
    case .some: return false
    }
}

/// 判断可选类型是否有值
public func isValue(_ any: Any?) -> Bool {
    return isNil(any) == false
}

/// for Array
public func isEmpty(_ value: [Any]?) -> Bool {
    return isNil(value) || value!.count == 0
}

/// for Array
public func isNotEmpty(_ value: [Any]?) -> Bool {
    return isEmpty(value) == false
}

/// for Dictionary
public func isEmpty(_ value: [AnyHashable: Any]?) -> Bool {
    return isNil(value) || value!.count == 0
}

/// for Dictionary
public func isNotEmpty(_ value: [AnyHashable: Any]?) -> Bool {
    return isEmpty(value) == false
}

/// for String
public func isEmpty(_ value: String?) -> Bool {
    return isNil(value) || value!.count == 0
}

/// for String
public func isNotEmpty(_ value: String?) -> Bool {
    return isEmpty(value) == false
}
