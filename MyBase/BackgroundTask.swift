//
//  BackgroundTask.swift
//
//
//  Created by Harvey on 2023/12/16.
//

import UIKit

/// 后台任务(可以在主线程/子线程运行)
///
/// 根据Apple不明确文档，不超过10分钟，实际上发现不同设备时间有所差异，
/// 如果超过限定时间任务未完成，会被系统结束
///
///  ### Example
///
/// ```Swift
/// Async.main.do {
///     BackgroundTask.begin()
///     sleep(10)
///     BackgroundTask.end()
/// }
/// ```
public struct BackgroundTask {
    private static var taskIdentifier: UIBackgroundTaskIdentifier = .invalid
    private static var taskTime: TimeInterval = 0
    private static let app =  UIApplication.shared
    
    public static func begin() {
        
        end()
        
        Self.taskTime = Date().timeIntervalSince1970
        Self.taskIdentifier = app.beginBackgroundTask {
            app.endBackgroundTask(Self.taskIdentifier)
            Self.taskIdentifier = .invalid
            Logger.verbose("后台任务过期(\(Date().timeIntervalSince1970 - Self.taskTime)秒).")
        }
    }
    
    public static func end() {
        guard Self.taskIdentifier != .invalid else { return }
        app.endBackgroundTask(Self.taskIdentifier)
        Self.taskIdentifier = .invalid
        Logger.verbose("后台任务正常结束(\(Date().timeIntervalSince1970 - Self.taskTime)秒).")
    }
}
