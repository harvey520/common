//
//  HttpRequest.swift
//
//
//  Created by yaozuopan on 2023/10/23.
//

import Alamofire

public extension HttpRequest {
    
    struct Path {
        public let rawValue: String
        public init(_ rawValue: String) {
            self.rawValue = rawValue
        }
    }
}

open class SessionProvider: Session {
        
    static let instance = SessionProvider(
        interceptor: Interceptor(adaptHandler: adaptHandler,
                                 retryHandler: retryHandler))
    
    /// 超时, 默认 60秒
    /// 如需更改，请重写此属性
    public static var timeoutInterval: TimeInterval {
        return 60.0
    }
    
    /// 拦截器
    /// 用于重写
    public static var adaptHandler: AdaptHandler {
        { (urlRequest, session, completion) in
            var varRequest = urlRequest
            varRequest.timeoutInterval = timeoutInterval
            completion(.success(varRequest))
        }
    }
    
    /// 重试处理
    /// 用于重写
    public static var retryHandler: RetryHandler {
        { (request, session, error, completion) in
            completion(.doNotRetry)
        }
    }
}

fileprivate struct HttpKeeper {
    
    private static var https: [AnyObject] = []
    
    static func append(_ item: AnyObject) {
        https.append(item)
    }
    
    static func remove(_ item: AnyObject) {
        https.removeAll { item === $0 }
    }
    
    private init() {}
}

public struct Encoding  {
    
    let rawValue: ParameterEncoding
    init(_ rawValue: ParameterEncoding) {
        self.rawValue = rawValue
    }

    public static let url = Encoding(URLEncoding.default)
    public static let json = Encoding(JSONEncoding.default)
}

public class HttpRequest<T: Codable> {
    
    public enum Action {
        case request
        case upload
        case download
    }
    
    public private(set) var action: Action = .request
    
    private var method: HTTPMethod
    public init(_ method: HTTPMethod = .get) {
        self.method = method
    }
    
    open var session: Session {
        return SessionProvider.instance
    }
   
    /// Host
    /// 如需更改，请重写此属性
    open var host: String {
        return "http://devimages.apple.com.edgekey.net/assets/elements/icons/128x128/xcode.png"
    }
    
    private var path: Path!
    @discardableResult
    public func path(_ value: Path) -> Self {
        path = value
        return self
    }
    
    private var encoding: ParameterEncoding = URLEncoding.default
    @discardableResult
    public func encoding(_ value: Encoding) -> Self {
        encoding = value.rawValue
        return self
    }
    
    public private(set) var parameters: Parameters = [:]
    @discardableResult
    public func parameters(_ value: Parameters) -> Self {
        parameters = parameters + value
        return self
    }
    
    private var queryParameters: Parameters = [:]
    @discardableResult
    public func queryParameters(_ value: Parameters) -> Self {
        queryParameters = value
        return self
    }
    
    private var queryString: String {
        return queryParameters
            .map { "\($0.0)=\($0.1)" }
            .joined(separator: "&")
    }
    
    private var pathParameters: Parameters = [:]
    @discardableResult
    public func pathParameters(_ value: Parameters) -> Self {
        pathParameters = value
        return self
    }
    
    /// 用于参数加密
    open func parameterSecure() -> Parameters {
//        switch action {
//        case .request: return parameters
//        default:
//            break
//        }
        return parameters
    }
    
    /// For Download
    private var destination: DownloadRequest.Destination?
    public func destination(_ value: @escaping DownloadRequest.Destination) -> Self {
        destination = value
        return self
    }
    
    /// For Upload
    private var uploadKeeper: UploadKeeper!
    public func upload(_ value: UploadKeeper) -> Self {
        action = .upload
        uploadKeeper = value
        return self
    }
    
    /// 发生错误则回调
    private var onError: ((Error) -> Void)?
    @discardableResult
    public func onError(_ value: @escaping (Error) -> Void) -> Self {
        onError = value
        return self
    }
    
    /// 当服务器返回数据并解析成功，则回调，其他情况不回调
    private var success: ((T) -> Void)?
    public func response(success handler: @escaping (T) -> Void) {
        success = handler
        buildResponse()
    }
    
    /// 发生错误或成功都会回调
    private var completion: ((Error?, AFDataResponse<T>, T?) -> Void)?
    public func response(completion handler: @escaping (Error?, AFDataResponse<T>, T?) -> Void) {
        completion = handler
        buildResponse()
    }
    
    private weak var dataRequest: DataRequest?
    private func buildResponse() {
        HttpKeeper.append(self)
        
        switch action {
        case .request: dataRequest = requestResponse()
        case .upload: dataRequest = uploadResponse()
        case .download: break
        }
    }
    
    private func requestResponse() -> DataRequest {
        session.request(blendURL(),
                        method: method,
                        parameters: parameterSecure(),
                        encoding: encoding)
        .responseDecodable(of: T.self, completionHandler: responseHandler)
    }
    
    
    private func uploadResponse() -> DataRequest {
        session.upload(multipartFormData: { [unowned self] multipartFormData in
            uploadKeeper.datas.forEach { data in
                multipartFormData.append(data.data,
                                         withName: uploadKeeper.keyName,
                                         fileName: data.name,
                                         mimeType: uploadKeeper.mimeType)
            }
        }, to: blendURL())
        .responseDecodable(of: T.self, completionHandler: responseHandler)
    }
    
    var responseHandler: (AFDataResponse<T>) -> Void {
        { [unowned self] result in
            Logger.network("Body => ", result.request?.httpBody?.string ?? "")
            
            if let data = result.data, let string = String(data: data, encoding: .utf8) {
                Logger.network("response => \(string)")
            }
            
            if let error = result.error {
                onError?(error)
                completion?(error, result, nil)
                Logger.error(error)
                return HttpKeeper.remove(self)
            }
            
            if let value = result.value {
                success?(value)
            }
            
            completion?(nil, result, result.value)
            HttpKeeper.remove(self)
        }
    }
    
    private func blendURL() -> String {
        var urlString = host + path.rawValue
        
        pathParameters.forEach { (key: String, value: Any) in
            urlString = urlString.replace(key, "\(value)")
        }
        
        if isNotEmpty(queryString) {
            urlString = urlString + "?\(queryString)"
        }
    
        return urlString
    }
}

/// Download
public extension HttpRequest {
    
    /// 当下载成功回调
     func download(_ success: @escaping (URL, Data) -> Void) {
         download { error, url, data in
             guard let _ = url, let _ = data else { return }
             success(url!, data!)
         }
    }
    
    /// 当下载完成回调(发生错误或成功)
    func download(_ completion: @escaping (Error?, URL?, Data?) -> Void) {
        action = .download
        HttpKeeper.append(self)
        
        session.download(blendURL(),
                         method: method,
                         parameters: parameterSecure(),
                         encoding: encoding,
                         to: destination)
        .response { [unowned self] download in
            if let error = download.error {
                onError?(error)
                completion(error, nil, nil)
                Logger.error(error)
                return HttpKeeper.remove(self)
            }
            
            guard let fileURL = download.fileURL else {
                completion(nil, nil, nil)
                return HttpKeeper.remove(self)
            }
            
            guard let data = try? Data(contentsOf: fileURL) else {
                completion(nil, fileURL, nil)
                return HttpKeeper.remove(self)
            }
            
            completion(nil, fileURL, data)
            HttpKeeper.remove(self)
        }
    }
}

public class UploadKeeper {
    
    public typealias UploadData = (name: String, data: Data)
//    public typealias UploadFile = (name: String, fileURL: URL)
    
    var datas: [UploadData] = []
    @discardableResult
    public func datas(_ value: [UploadData]) -> Self {
        datas = value
        return self
    }
    
//    var files: [UploadFile] = []
//    @discardableResult
//    public func files(_ value: [UploadFile]) -> Self {
//        files = value
//        return self
//    }
//    
    var mimeType: String!
    @discardableResult
    public func mimeType(_ value: String) -> Self {
        mimeType = value
        return self
    }
    
    /// 这个要服务器定义的一样，否则服务器无法识别
    var keyName: String!
    @discardableResult
    public func keyName(_ value: String) -> Self {
        keyName = value
        return self
    }
}
