//
//  Number.swift
//  MyBase
//
//  Created by Harvey on 2021/5/18.
//

import UIKit

// MARK: - Extension Int
extension Int {
    
    /// 转成时间格式，六十进制格式显示
    public var time: String {
        let remainderSecond = quotientAndRemainder(dividingBy: 60)
        let remainderMinute = remainderSecond.quotient.quotientAndRemainder(dividingBy: 60)
        let remainderHour = remainderMinute.quotient.quotientAndRemainder(dividingBy: 60)
        
        guard remainderSecond.quotient > 0 else {
            return String(format: "%02d", remainderSecond.remainder)
        }
        
        guard remainderMinute.quotient > 0 else {
            return String(format: "%02d:%02d", remainderMinute.remainder, remainderSecond.remainder)
        }
        
        return String(format: "%02d:%02d:%02d", remainderHour.remainder, remainderMinute.remainder, remainderSecond.remainder)
    }
}

public extension BinaryInteger {
    var float: Float { Float(self) }
    var double: Double { Double(self) }
}

public extension BinaryFloatingPoint {
    var upInt: Int { Int(ceil(self)) }
    var int: Int { Int(self) }
}

public extension String {
    var int: Int { Int(double) }
    var float: Float { Float(double) }
    var double: Double { Double(self) ?? 0.0 }
    
    /// hex string to Int
    var hexInt: Int {
        var intValue: Int64 = 0
        Scanner(string: self).scanHexInt64(&intValue)
        return Int(intValue)
    }
}

public extension Float {
    var double: Double { Double(self) }
    
    /// decimal 保留几位小数
    func string(_ decimal: Int) -> String {
        return String(format: "%.\(decimal)f", self)
    }
}

public extension Double {
    var float: Float { Float(self) }
    
    /// decimal 保留几位小数
    func string(_ decimal: Int) -> String {
        return String(format: "%.\(decimal)f", self)
    }
}

public extension Numeric {
    var string: String { "\(self)" }
}

public extension Strideable {
    
    func intercept(_ max: Self) -> Self {
        return self > max ? max : self
    }
    
    func checkRange(min: Self, max: Self) -> Self {
        guard self > max else { return self < min ? min : self }
        return max
    }
}
