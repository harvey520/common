//
//  Weakly.swift
//  MyBase
//
//  Created by yaozuopan on 2024/6/28.
//

import Foundation
 
public class Weakly<T: AnyObject> {
    public private(set) weak var weakObject: T?
    
    public init(_ weakObject: T?) {
        self.weakObject = weakObject
    }
    
    /// true = weakObject 未释放
    public var isValue: Bool {
        isNil == false
    }
    
    /// true = weakObject 已被释放
    public var isNil: Bool {
        weakObject == nil
    }
}
