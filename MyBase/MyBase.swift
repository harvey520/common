//
//  MyBase.swift
//  MyBase
//
//  Created by Harvey on 2024/1/19.
//

import Foundation
import UIKit

public typealias None = ()
public typealias Work<T, B> = (T) -> B

public protocol CommonNumber {
    var double: Double { get }
}

extension Int: CommonNumber { }
extension Float: CommonNumber { }
extension Double: CommonNumber {
    public var double: Double { self }
}

extension CGFloat: CommonNumber {
    public var double: Double { self }
}

public extension NSObjectProtocol {
    var className: String {
        "\(Self.self)"
    }
}

public extension CGRect {
    static func only(x: CommonNumber = 0.0,
                     y: CommonNumber = 0.0,
                     width: CommonNumber = UIScreen.main.bounds.size.width,
                     height: CommonNumber = UIScreen.main.bounds.size.height) -> Self {
        Self.init(x: x.double, y: y.double, width: width.double, height: height.double)
    }
    
    static let fullScreen = CGRect(origin: .zero, size: UIScreen.main.bounds.size)
}

/// 透明度转成十六进制
///
/// - Parameter value: Range [0.0, 1.0]
/// - Returns: hex string
public func hexAlpha(_ value: Float) -> String {
    return String(format: "%02x", Int(255.0 * value))
}

private let scale: Double = {
    let size = UIScreen.main.bounds.size
    return min(size.width, size.height) / 375.0
}()

public extension CommonNumber {
    
    /// 以 375.0 屏幕为基准适配屏幕,数值按比例放大，仅支持 iPhone
    var fit: Double {
        return ceil(fitFont)
    }
    
    var fitFont: Double {
        return self.double * scale
    }
}

