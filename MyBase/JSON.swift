//
//  JSON.swift
//  MyBase
//
//  Created by Harvey on 2024/1/7.
//

import Foundation

public struct JSON { }

public extension Dictionary {
    
    func jsonString(options: JSONSerialization.WritingOptions = [.fragmentsAllowed]) -> String? {
        guard JSONSerialization.isValidJSONObject(self) else {
            Logger.debug("无效的 JSONObject")
            return nil
        }
        guard let data = try? JSONSerialization.data(withJSONObject: self, options: options) else { return nil }
        return data.string
    }
}

public extension Array {
    
    func jsonString(options: JSONSerialization.WritingOptions = [.fragmentsAllowed]) -> String? {
        guard JSONSerialization.isValidJSONObject(self) else {
            Logger.debug("无效的 JSONObject")
            return nil
        }
        guard let data = try? JSONSerialization.data(withJSONObject: self, options: options) else { return nil }
        return data.string
    }
}

public extension String {
    
    func jsonObject(options: JSONSerialization.ReadingOptions = [.mutableContainers]) -> Any? {
        guard let data = self.data else { return nil }
        return data.jsonObject(options: options)
    }
}

public extension Data {
    
    func jsonObject(options: JSONSerialization.ReadingOptions = [.mutableContainers]) -> Any? {
        do { return try JSONSerialization.jsonObject(with: self, options: options) }
        catch { Logger.error(error) }
        return nil
    }
}

// MARK: - Extension Codable

public extension Encodable {
    
    /// JSON encode
    var encode: Data? {
        do {
            return try JSONEncoder().encode(self)
        } catch { Logger.error(error) }
        return nil
    }
    
    var jsonString: String? {
        encode?.string
    }
}

public extension Decodable {
    
    /// JSON decode
    static func decode(_ from: Data) -> Self? {
        do {
            return try JSONDecoder().decode(Self.self, from: from)
        } catch { Logger.error(error) }
        return nil
    }
    
    /// JSON decode
    static func decode(_ from: String) -> Self? {
        guard let data = from.data else { return nil }
        return decode(data)
    }
}

public extension JSON {
    /// JSON decode
    static func decode<T: Decodable>(_ decodable: T.Type, from data: Data) -> T? {
        do {
            return try JSONDecoder().decode(decodable.self, from: data)
        } catch { Logger.error(error) }
        return nil
    }
    
    /// JSON decode
    static func decode<T: Decodable>(_ decodable: T.Type, from jsonString: String) -> T? {
        guard let data = jsonString.data else { return nil }
        return decode(decodable, from: data)
    }
}

public extension Array where Element: Encodable {
    /// JSON encode
    var jsonData: Data? {
        do {
            return try JSONEncoder().encode(self)
        } catch { Logger.error(error) }
        return nil
    }
    
    var jsonString: String? {
        encode?.string
    }
}
