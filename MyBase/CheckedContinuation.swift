//
//  CheckedContinuation.swift
//  MyBase
//
//  Created by Harvey on 2024/1/14.
//

import Foundation

/// 旧式回调转 async/await 新式回调，解决地狱链式回调问题
///
/// ### Example
///
/// ```swift
/// Task {
///     let result: Int = await continuation {
///         sleep(3)
///         return 1
///     }
///     print(result)
/// }
/// ```
public func continuation<T>(work: () -> T) async -> T {
    return await withCheckedContinuation { checked in
        checked.resume(returning: work())
    }
}

/// 旧式回调转 async/await 新式回调，解决地狱链式回调问题
///
/// ### Example
///
/// ```swift
/// Task {
///     let result: Int = await continuation { checked in
///         sleep(3)
///         checked.resume(returning: 1)
///     }
///     print(result)
/// }
/// ```
public func continuation<T>(work: (_ checked: CheckedContinuation<T, Never>) -> Void) async -> T {
    return await withCheckedContinuation { checked in
        work(checked)
    }
}

/// 旧式回调转 async/await 新式回调，可抛出异常，解决地狱链式回调问题
///
/// ### Example
///
/// ```swift
/// Task {
///     do {
///         let result: Int = try await throwingContinuation { checked in
///             sleep(3)
///             checked.resume(throwing: AFError.explicitlyCancelled)
///         }
///         print(result)
///     } catch {
///         print(error)
///     }
/// }
/// ```
public func throwingContinuation<T>(work: (_ checked: CheckedContinuation<T, Error>) -> Void) async throws -> T {
    return try await withCheckedThrowingContinuation { checked in
        work(checked)
    }
}
