//
//  ViewController.swift
//  Example
//
//  Created by Harvey on 2021/5/18.
//

import UIKit
import MyBase
import CryptoKit
import Alamofire
import RxSwift

class PageRoutingWriter: FileWriter {
    
    var fileHandle: FileHandle!
    
    static let shared: PageRoutingWriter = .init()
    
    var maxFileCount: Int { 2 }
    var suffix: String { "log" }
    
    required init() {
        prepare()
    }
    
    func fileContent(_ name: String) -> [String] {
        []
    }
    
    func append(_ value: String?) -> Bool {
        false
    }
}

extension Notify.Name {
    static let text = Notify.Name("ldldldl")
}

class MyData: Codable {
    var name: String = ""
    var age: Int = 0
}


class TestClass: Storable {
    
    required init(_ uuid: String) {
        self.uuid = uuid
    }
    
    
    static var all: [TestClass] = fetchAll()

    var uuid: String = .uuid()
    
    static var groupName: String { "数据隔离" }
}

class MyTestClass {
    
}

class MyTool: Manageable {
    var folder: String { App.Path.document + "/Download" }
    var suffix: String { "zip" }
    
    init() { createFolder() }
}

class ViewController: UIViewController {
    
    private let label = UITextView()
    private let string = "For more information"
    
    var back: Work<None, Void>?
    private var array: [Weakly<MyTestClass>] = []
    
//    private let string1 = "For more information"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .white
        
        array.append(Weakly(MyTestClass()))
        
        array.removeAll(where: { $0.isNil })
        
        let gg = 1000
        Hardware.Unit(UInt64(gg))
        
        let width: CGFloat = 300.0
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.minimumLineHeight = 17.0
//        
//        let (size, attString) = string1.box(width, attributes: [
//            .font: UIFont.boldSystemFont(ofSize: 14),
//            .backgroundColor: UIColor.purple,
//            .paragraphStyle: paragraphStyle
//        ])
//        print(size)
//
        PageRoutingWriter.write("kfdkfkewklrfjekw")
        let dict: [String: Any]? = nil
        
        print(TestClass.groupName)
        
//        label.attributedText = attString
        label.textColor = .red
//        label.numberOfLines = 0
        label.isUserInteractionEnabled = true
        label.backgroundColor = .gray
        label.frame = CGRect(x: 30, y: 100, width: width, height: 50)
        view.addSubview(label)
        
        Notify.post(.AVAssetChapterMetadataGroupsDidChange, object: nil, userInfo: ["d": "ddd"])
        Notify()
            .name(.AVAssetChapterMetadataGroupsDidChange)
            .observer(self)
            .subscribe { notification in
                print("ddd")
            }
        Notify()
            .name(.AVAssetChapterMetadataGroupsDidChange)
            .observer(self)
            .subscribe { notification in
                
            }
        
//        print(URL(string: "https://www.baidu.com/index.html")!.queryItems)
        
      
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let int = 180
        let intf: Float = 180
        let cgf: CGFloat = 180
        let doublef = 180.0
        print(int.fit, intf.fit, cgf.fit, doublef.fit)
        
        label.attributedText = string.add(
            .attributes
                .font(.boldSystemFont(ofSize: 30))
                .foregroundColor(.purple))
            .add(.item.text("more")
                .link("https://www.baidu.com")
                .underlineStyle(.single)
                .foregroundColor(.red))
            
        print(string.size(.fit.height(300.0), attrs: .attributes.font(.boldSystemFont(ofSize: 20))))
            
//        Parse.start()
        Observable<Int>.timer(.milliseconds(300),
                                    period: .milliseconds(350),
                              scheduler: Scheduler.main)
             
        
        let f: Int = 1666
        let ff: Float = 18888.1118
        let fff: Double = 1999333339999.122233322

//        print(f.string, f, f.float, f.double)
//        print(ff.string, ff.int, ff, ff.double)
//        print(fff.string, fff.int, fff.float, fff)
//        
        print(ff.string(2), fff.string(5))
    }
    
    func onAction(){
        let str = "#eebbff"
        print("\(str.md5)")
    }
}

