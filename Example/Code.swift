//
//  Code.swift
//  Example
//
//  Created by Harvey on 2024/1/14.
//

import Foundation
import MyBase

let code =
"""
    /************************ Attributes ************************/
    @available(iOS 6.0, *)
    public static let font: NSAttributedString.Key

    @available(iOS 6.0, *)
    public static let paragraphStyle: NSAttributedString.Key // NSParagraphStyle, default defaultParagraphStyle

    @available(iOS 6.0, *)
    public static let foregroundColor: NSAttributedString.Key // UIColor, default blackColor

    @available(iOS 6.0, *)
    public static let backgroundColor: NSAttributedString.Key // UIColor, default nil: no background

    @available(iOS 6.0, *)
    public static let ligature: NSAttributedString.Key // NSNumber containing integer, default 1: default ligatures, 0: no ligatures

    @available(iOS 6.0, *)
    public static let kern: NSAttributedString.Key // NSNumber containing floating point value, in points; amount to modify default kerning. 0 means kerning is disabled.

    @available(iOS 14.0, *)
    public static let tracking: NSAttributedString.Key // NSNumber containing floating point value, in points; amount to modify default tracking. 0 means tracking is disabled.

    @available(iOS 6.0, *)
    public static let strikethroughStyle: NSAttributedString.Key // NSNumber containing integer, default 0: no strikethrough

    @available(iOS 6.0, *)
    public static let underlineStyle: NSAttributedString.Key // NSNumber containing integer, default 0: no underline

    @available(iOS 6.0, *)
    public static let strokeColor: NSAttributedString.Key // UIColor, default nil: same as foreground color

    @available(iOS 6.0, *)
    public static let strokeWidth: NSAttributedString.Key // NSNumber containing floating point value, in percent of font point size, default 0: no stroke; positive for stroke alone, negative for stroke and fill (a typical value for outlined text would be 3.0)

    @available(iOS 6.0, *)
    public static let shadow: NSAttributedString.Key // NSShadow, default nil: no shadow

    @available(iOS 7.0, *)
    public static let textEffect: NSAttributedString.Key // NSString, default nil: no text effect

    
    @available(iOS 7.0, *)
    public static let attachment: NSAttributedString.Key // NSTextAttachment, default nil

    @available(iOS 7.0, *)
    public static let link: NSAttributedString.Key // NSURL (preferred) or NSString

    @available(iOS 7.0, *)
    public static let baselineOffset: NSAttributedString.Key // NSNumber containing floating point value, in points; offset from baseline, default 0

    @available(iOS 7.0, *)
    public static let underlineColor: NSAttributedString.Key // UIColor, default nil: same as foreground color

    @available(iOS 7.0, *)
    public static let strikethroughColor: NSAttributedString.Key // UIColor, default nil: same as foreground color
"""

struct Parse {
    static func start() {
        let newCode = code
            .split(separator: "\n")
            .filter { $0.hasPrefix("    public static let ") }
            .map { ($0 as NSString).components(separatedBy: " // ") }
            .map { items -> [String] in
                let key = items.first!
                let doc = items.last ?? ""
                return [key.replace("    public static let ", "").replace(": NSAttributedString.Key", ""), isNotEmpty(doc) ? "//\(doc)" : ""]
            }
            .map { items in
              let string = """
    {doc}
    @discardableResult
    func {name}(_ value: UIFont) -> Self {
        attributes[.{name}] = value
        return self
    }
"""
                return string.replace("{name}", items.first!).replace("{doc}", items.last!)
            }
            .joined(separator: "\n\n")
        print(newCode)
    }
}

