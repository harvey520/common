//
//  AppDelegate.swift
//  Example
//
//  Created by Harvey on 2021/5/18.
//

import UIKit
import MyBase
import UserNotifications

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
//        Logger.isDebug = true
//        Logger.allowTypes = Logger.all
//        
//        Logger.saveHandler = { log, type in
////            print("save", log)
//        }
        
        window = UIWindow(frame: UIScreen.main.bounds)
//        Logger.verbose(window!.safeAreaInsets)

        let tabBar = UITabBarController()
        tabBar.viewControllers = [
            UINavigationController(rootViewController: ViewController()),
            UINavigationController(rootViewController: ViewController()),
            UINavigationController(rootViewController: ViewController())
        ]
        window?.rootViewController = tabBar//ViewController()
        window?.makeKeyAndVisible()
        
        
        return true
    }
}

